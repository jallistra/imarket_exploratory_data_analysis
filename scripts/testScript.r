# ------------------------------------------------------------ #
# Ubiqum Module 1, Sprint 1
# Belkin vs Elago Survey: Exploratory Data Analysis
# Author: Danielle Jeffery
# Date: January 2020
# Version: 0.1
# ------------------------------------------------------------ #

# --- load libraries ----
library(ggplot2)
library(tidyverse)
library(viridis)
library(grid)

# --- read data ----
surveyData <- read.csv2("data/surveyBelkinElago.csv")

# --- exploration ----
summary(surveyData)
git clone https://jallistra@bitbucket.org/jallistra/imarket-exploratory-data-analysis.gittypeof(surveyData)
levels(surveyData)
class(surveyData)

# search for missing values
sum(is.na(surveyData))

# --- data conversions ---

# change zipcode column name to region
names(surveyData)[names(surveyData) == "zipcode"] <- "region"

# change blank fields in brand column to "No Response"
surveyData$brand <- revalue(surveyData$brand, c(" " ="No Response"))

# convert factoral fields from numeric
surveyData$elevel <- factor(
                       surveyData$elevel,
                       labels = c(
                         "High School Degree or Lower",
                         "Professional Diploma",
                         "College Degree",
                         "Master's, Doctoral or Professional Degree"
                       )
                     )

surveyData$car <- factor(
                     surveyData$car, labels = c(
                       "BMW",
                       "Tesla",
                       "Volkswagen",
                       "Fiat",
                       "Chrysler",
                       "Citroen1",
                       "Ford",
                       "Honda",
                       "Opel",
                       "Citroen2",
                       "Porsche",
                       "Peugeot",
                       "Audi",
                       "Mercedes Benz",
                       "Kia",
                       "Nissan",
                       "Hyunday",
                       "Renault",
                       "Toyota",
                       "Dacia"
                     )
                  )

surveyData$region <- factor(
                        surveyData$region, labels = c(
                          "Central Europe",
                          "East-Central Europe",
                          "Eastern Europe",
                          "Northeastern Europe",
                          "Northern Europe",
                          "Northwestern Europe",
                          "Southeastern Europe",
                          "Southwestern Europe",
                          "Western Europe"
                        ))

# --- plot distributions by brand ---
surveyDataSubset = subset(surveyData,brand != "No Response")

# Brand distribution
surveyDataSubset %>% 
  count(brand, sort = TRUE) %>% 
  ggplot(aes(brand, n, fill = brand)) + 
  geom_col() + 
  theme_minimal() + 
  guides(fill = "none") + 
  labs(x = NULL, y = "Count") + 
  scale_fill_manual(values=c("#0392cf", "#7bc043")) + 
  theme(axis.text.x = element_blank()) + 
  scale_y_continuous(breaks = seq(0, 6000, 500)) + 
  geom_text(aes(label = n, y = n - 300))

# Age Distribution by Brand
surveyDataSubset %>% 
  group_by(brand) %>%  
  count(age) %>% 
  ggplot(aes(x = age, y = n, fill=brand)) + 
  geom_col() + 
  theme_minimal() + 
  guides(fill="none") + 
  scale_fill_manual(values = c("#0392cf", "#7bc043")) + 
  scale_x_continuous(breaks = seq(0, 80, 5)) + 
  labs(y = NULL) + 
  scale_y_continuous(breaks = seq(0, 230, 20)) +
  facet_grid(cols = vars(brand)) +
  theme(strip.background = element_blank(), 
        strip.text = element_blank())

# Salary Distribution by Brand
qplot(
  x = salary,
  data = surveyDataSubset,
  binwidth = 1000,
  fill = brand
) + theme_classic() + 
  theme_minimal() + 
  guides(fill = "none") + 
  scale_fill_manual(values = c("#0392cf", "#7bc043")) + 
  scale_x_continuous(breaks = seq(0, 150000, 20000)) +
  scale_y_continuous(breaks = seq(0, 150, 10)) +
  facet_grid(cols = vars(brand)) + 
  theme(strip.background = element_blank(), 
        strip.text = element_blank())

# Salary Distribution (box plot) by Brand
ggplot(surveyDataSubset, aes(brand, salary, fill=brand)) + 
  geom_boxplot() + 
  theme_classic() + 
  guides(fill="none") + 
  labs(title = "Salary Distribution By Brand", subtitle = "subtitle") + 
  theme(plot.title = element_text(hjust = 0.5))

# Education Level Distribution by Brand
ggplot(surveyDataSubset, aes(elevel, fill=brand)) + 
  geom_bar(position="dodge") + 
  theme_classic() + 
  guides(fill="none") + 
  coord_flip() + 
  theme(axis.title.y = element_blank(), 
        strip.background = element_blank(), 
        strip.text = element_blank()) + 
  scale_fill_manual(values=c("#0392cf", "#7bc043"))

# Car Distribution by Brand
ggplot(surveyDataSubset, aes(car, fill=brand)) + 
  geom_bar(data = subset(surveyDataSubset, brand == "Elago")) + 
  geom_bar(data = subset(surveyDataSubset, brand == "Belkin"), aes(y = ..count..*(-1))) + 
  scale_y_continuous(breaks = seq(-500,500,100), labels = abs(seq(-500,500,100))) + 
  guides(fill="none") +
  theme_minimal() + 
  coord_flip() +
  scale_fill_manual(values=c("#0392cf", "#7bc043"))

# Region Distribution by Brand
ggplot(surveyDataSubset, aes(region, fill=brand)) + 
  geom_bar(data = subset(surveyDataSubset, brand == "Elago")) + 
  geom_bar(data = subset(surveyDataSubset, brand == "Belkin"), aes(y = ..count..*(-1))) + 
  scale_y_continuous(breaks = seq(-500,500,100), labels = abs(seq(-500,500,100))) + 
  theme_classic() + 
  theme(axis.title.y = element_blank(),
        strip.background = element_blank(), 
        strip.text = element_blank()) +
  guides(fill="none") + 
  scale_fill_manual(values=c("#0392cf", "#7bc043")) +
  coord_flip()

# Credit Distribution by Brand
ggplot(surveyDataSubset, aes(credit, fill = brand)) +
  geom_histogram(binwidth = 5) +
  scale_fill_manual(values=c("#0392cf", "#7bc043")) +
  theme_minimal() +
  theme(strip.background = element_blank(), 
        strip.text = element_blank()) +
  labs(x = "credit rating") +
  guides(fill = "none") +
  scale_x_continuous(breaks = seq(400, 950, 50)) +
  scale_y_continuous(breaks = seq(0, 350, 50)) +
  facet_grid(cols = vars(brand))

# ---- plot relationships ----

# Age and Salary by Brand
ggplot(surveyDataSubset, aes(age, salary, colour = brand)) + 
  geom_point() + 
  theme_minimal() + 
  scale_x_continuous(breaks = seq(20, 80, 5)) + 
  scale_y_continuous(breaks = seq(20000, 160000, 10000)) +
  theme(strip.background = element_blank(), 
        strip.text = element_blank(), 
        legend.position = "none") + 
  scale_colour_manual(values = c("#0392cf", "#7bc043")) +
  facet_grid(cols = vars(brand))

# Age and Credit by Brand
ggplot(surveyDataSubset, aes(age, credit, colour=brand)) + 
  geom_point() +
  theme_minimal() + 
  theme(strip.background = element_blank(), 
        strip.text = element_blank(),
        legend.position = "none") +
  scale_colour_manual(values = c("#0392cf", "#7bc043")) +
  scale_x_continuous(breaks = seq(0, 90, 10)) + 
  scale_y_continuous(breaks = seq(400, 850, 50)) + 
  facet_grid(cols = vars(brand))

#car_salary_bar <- surveyDataSubset %>% group_by(car, brand) %>% summarise(meanSalary = mean(salary)) %>% arrange(desc(meanSalary)) %>% ggplot(aes(car, meanSalary, fill=brand)) + geom_col(position="dodge") + coord_flip()
#car_salary_bar

# Car vs Salary Boxplot
ggplot(surveyDataSubset, aes(car, salary, fill = brand)) + 
  geom_boxplot() + 
  coord_flip() + 
  facet_grid(cols = vars(brand)) + 
  theme_minimal() + 
  ggtitle("Cars and Salary For Each Brand")  + 
  theme(plot.title = element_text(hjust = 0.5))# + reorder(car, mean(salary))

# Brand vs Age Boxplot
ggplot(surveyDataSubset, aes(brand, age, fill=brand)) + 
  geom_boxplot()

# Brand vs Education Count
ggplot(surveyDataSubset, aes(brand, elevel, colour=elevel)) + 
  geom_count()

# Brand vs Car Cound
ggplot(surveyDataSubset, aes(brand, car, colour=car)) + 
  geom_count()

# Brand vs Region Count
ggplot(surveyDataSubset, aes(brand, region, colour=region)) + 
  geom_count()

                                           